Commande pour cloner le répertoire
    - Ouvrez votre terminal puis placez vous dans le dossier où vous souhaitez cloner le répertoire;
    - Tapez la commande suivante :
    git clone https://gitlab.com/oliverakp/git4.rt4.s8.git
    - Vous venez de cloner le répertoire git4.rt4.s8.

Bon semestre (^_^) !
